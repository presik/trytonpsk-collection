# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from . import account, collection, configuration, ir, reports


def register():
    Pool.register(
        configuration.Configuration,
        configuration.AccountProcedure,
        configuration.Procedure,
        configuration.Level,
        collection.Tracking,
        collection.TrackingCampaign,
        collection.PartySegment,
        collection.PartySegmentLine,
        collection.Collection,
        collection.CreateCollectionStart,
        collection.Voucher,
        collection.TrackingReportStart,
        collection.PortfolioStatusStart,
        collection.BillCollectionStart,
        account.AccountReceivablePayableStart,
        ir.Cron,
        module='collection', type_='model')
    Pool.register(
        collection.CreateCollection,
        collection.TrackingReportWizard,
        collection.PortfolioStatus,
        collection.PortfolioStatus2,
        collection.BillCollection,
        account.AccountReceivablePayable,
        module='collection', type_='wizard')
    Pool.register(
        collection.TrackingReport,
        collection.PortfolioStatusReport,
        collection.BillCollectionReport,
        account.AccountReceivablePayableReport,
        reports.PortfolioStatusReport,
        module='collection', type_='report')
