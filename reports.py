# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import pandas as pd
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard


class PortfolioStatusStart(ModelView):
    "Portfolio Status Start"
    __name__ = 'collection.print_portfolio_status.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    detailed = fields.Boolean('Detailed')
    date_to = fields.Date('Date to')
    category_party = fields.MultiSelection('get_category_party', 'Party Categories')
    kind = fields.Selection([
        ('in', 'Supplier'),
        ('out', 'Customer'),
    ], 'Kind', required=True)
    payment_terms = fields.MultiSelection('get_payment_terms', 'Payment Term')

    procedures = fields.MultiSelection('get_procedures', "Procedures")

    @classmethod
    def get_procedures(cls):
        pool = Pool()
        Procedure = pool.get('collection.procedure')
        procedures = Procedure.search_read([], fields_names=['id', 'name'])
        return [(r['id'], r['name']) for r in procedures]

    @classmethod
    def get_category_party(cls):
        pool = Pool()
        PartyCategory = pool.get('party.category')
        categories = PartyCategory.search_read([('active', '=', True)],
            fields_names=['id', 'name'])
        return [(r['id'], r['name']) for r in categories]

    @classmethod
    def get_payment_terms(cls):
        pool = Pool()
        PaymentTerm = pool.get('account.invoice.payment_term')
        payment_terms = PaymentTerm.search_read([], fields_names=['id', 'name'])
        return [(r['id'], r['name']) for r in payment_terms]

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_group():
        return 'customers'


class PortfolioStatus(Wizard):
    "Portfolio Status"
    __name__ = 'collection.print_portfolio_status'

    start = StateView('collection.print_portfolio_status.start',
                      'collection.print_portfolio_status_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Print', 'print_', 'tryton-ok', default=True),
                          ])
    print_ = StateReport('collection.portfolio_status_report')

    def do_print_(self, action):
        accounts = set(a.id for p in self.start.procedures for a in p.accounts)
        data = {
            'ids': [],
            'company': self.start.company.id,
            'detailed': self.start.detailed,
            'kind': self.start.kind,
            'category_party': list(self.start.category_party),
            'procedures': list(self.start.procedures),
            'accounts': list(accounts),
            'payment_terms': list(self.start.payment_terms),
            'date_to': self.start.date_to,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PortfolioStatusReport(Report):
    "Portfolio Status Report"
    __name__ = 'collection.portfolio_status_report.new'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Line = pool.get('account.move.line')
        company = Company(data['company'])
        date_ = Pool().get('ir.date').today()
        if data['date_to']:
            date_ = data['date_to']
            data['date_to'] = data['date_to'].strftime("'%Y-%m-%d'")
        op = 'operation_center' in Line._fields
        data['has_operation_center'] = op
        cursor = Transaction().connection.cursor()
        print(op, ',,,,,,,')

        labels, intervals = cls.get_intervals(data)
        df_invoices = cls.get_data_collection(data, cursor, labels, intervals)
        df_opening_balances = cls.get_data_opening_balances(data, cursor, labels, intervals)

        if not data['detailed']:
            columns = ['TERCERO', 'ID DOCUMENTO', "VALOR FACTURA", "PAGO", "SALDO", *labels]
            df_invoices = df_invoices[columns]
            df_invoices = df_invoices.groupby(['TERCERO', 'ID DOCUMENTO'], as_index=False).sum()
            df_opening_balances = df_opening_balances[columns]
            df_opening_balances = df_opening_balances.groupby(['TERCERO', 'ID DOCUMENTO'], as_index=False).sum()

        totals = cls.get_totals(df_invoices, df_opening_balances, labels)
        report_context['cut_date'] = date_
        report_context['company'] = company.party.name
        report_context['records'] = df_invoices
        report_context['lines_without_inv'] = df_opening_balances
        report_context['totals'] = totals
        return report_context

    @classmethod
    def get_totals(cls, df_invoices, df_opening_balances, labels):
        columns = ["VALOR FACTURA", "PAGO", "SALDO"]
        columns.extend(labels)
        records_total = df_invoices[columns].sum().astype(float)
        records_opening_balances_total = df_opening_balances[columns].sum().astype(float)
        totals = records_total.add(records_opening_balances_total)
        num_columns = len(df_invoices.columns) - len(totals) - 1
        add = ['' for n in range(num_columns)] + ['TOTAL: ']
        totals = add + totals.tolist()
        return totals

    @classmethod
    def where_(cls, data):
        payment_term = ""
        if data.get('payment_term'):
            field_payment_term = tuple(data['payment_term'])
            if len(data['payment_term']) == 1:
                field_payment_term = str(tuple(data['payment_term'])).replace(',', '')
            payment_term = f" and i.payment_term in {field_payment_term}"

        categories = ""
        if data.get('category_party'):
            field_categories = tuple(data['category_party'])
            if len(data['category_party']) == 1:
                field_categories = str(tuple(data['category_party'])).replace(',', '')
            categories = f" and pcr.category in {field_categories} "

        accounts = ''
        if data.get('accounts'):
            field_accounts = tuple(data['accounts'])
            if len(data['accounts']) == 1:
                field_accounts = str(tuple(data['accounts'])).replace(',', '')

            accounts = f" and i.account in {field_accounts} "

        state = " and i.state='posted'"

        if data['date_to']:
            date_to = data['date_to']
            state = f" and i.state in ('posted', 'paid') and i.invoice_date <= {date_to}"

        where = f"""where i.company={data['company']} and i.type= '{data['kind']}'
            {payment_term} {categories} {accounts} {state}"""
        return where

    @classmethod
    def get_data_collection(cls, data, cursor, labels, intervals):

        where = cls.where_(data)
        query_invoices = cls.get_query_invoices(data, where)
        cursor.execute(query_invoices)
        data_invoices = cursor.fetchall()

        query_maturity = cls.get_query_maturity_invoice(data, where)
        cursor.execute(query_maturity)
        data_maturity = cursor.fetchall()

        query_payments = cls.get_query_payments(data, where)
        cursor.execute(query_payments)
        data_payments = cursor.fetchall()

        query_payments_reconcile = cls.get_query_payments_reconcile(data, where)
        cursor.execute(query_payments_reconcile)
        data_payments_reconcile = cursor.fetchall()

        df1 = pd.DataFrame(data_invoices, columns=['move', 'invoice', 'party', 'TERCERO', 'ID DOCUMENTO', 'CATEGORIA', 'NUM. FACTURA', 'PLAZO DE PAGO', 'FECHA FACTURA', 'VALOR FACTURA'])
        df2 = pd.DataFrame(data_maturity, columns=['move', 'FECHA VENCIMIENTO', 'DIAS VENCIDOS'])
        df3 = pd.DataFrame(data_payments, columns=['invoice', 'PAGO'])
        df4 = pd.DataFrame(data_payments_reconcile, columns=['invoice', 'PAGO'])

        payment_df = pd.merge(df3, df4, on='invoice', how='outer', suffixes=('_df3', '_df4'))
        payment_df['PAGO'] = payment_df['PAGO_df3'].fillna(0) + payment_df['PAGO_df4'].fillna(0)
        payment_df.drop(columns=['PAGO_df3', 'PAGO_df4'], inplace=True)

        merged_df = pd.merge(df1, df2, on='move', how='left')
        merged_df = pd.merge(merged_df, payment_df, on='invoice', how='left')
        # print(merged_df)
        merged_df['PAGO'] = merged_df['PAGO'].fillna(0)
        merged_df['FECHA FACTURA'] = merged_df['FECHA FACTURA'].astype(str)
        merged_df['FECHA VENCIMIENTO'] = merged_df['FECHA VENCIMIENTO'].astype(str)
        merged_df['SALDO'] = merged_df['VALOR FACTURA'] + merged_df['PAGO']
        merged_df = merged_df.loc[merged_df['SALDO'] > 0]

        # Crear una nueva columna con las etiquetas de los intervalos
        merged_df['interval'] = pd.cut(merged_df['DIAS VENCIDOS'], bins=intervals, labels=labels, right=False)

        # Crear columnas separadas para cada intervalo y asignar el valor de factura
        for label in labels:
            merged_df[label] = merged_df['SALDO'] * (merged_df['interval'] == label).astype(int)
        columns = [
            'TERCERO', 'ID DOCUMENTO', 'CATEGORIA', 'NUM. FACTURA', 'PLAZO DE PAGO',
            'FECHA FACTURA', 'FECHA VENCIMIENTO', 'DIAS VENCIDOS',
            'VALOR FACTURA', 'PAGO', 'SALDO',
        ]
        columns.extend(labels)
        merged_df = merged_df.reindex(columns=columns)
        return merged_df

    @classmethod
    def get_intervals(cls, data):
        pool = Pool()
        Config = pool.get('collection.configuration')
        ages = Config(1).age_collection.split(',')
        labels = []
        intervals = [-float('inf')]
        last = ages[-1]
        before = None
        for n in ages:
            if not before:
                labels.append(n + ' días')
            else:
                labels.append(before + n.strip() + ' días')
            if n == last:
                labels.append('> ' + n.strip() + ' días')
            before = str(int(n) + 1) + '-'
            intervals.append(int(n))
        intervals.append(float('inf'))
        return labels, intervals

    @classmethod
    def get_query_invoices(cls, data, where):
        return f"""WITH
            moves AS (
                SELECT
                    id, date, SPLIT_PART(origin, ',', 2)::INTEGER as invoice
                FROM
                    account_move
                WHERE state='posted' and origin LIKE 'account.invoice,%'),
            party_category_rel AS (SELECT DISTINCT ON (party) party, category from party_category_rel)
            SELECT m.id, m.invoice, i.party, p.name, p.id_number, pc.name as category,  i.number,  pt.name, i.invoice_date, sum(ml.debit- ml.credit)
            FROM
                account_move_line as ml
                LEFT JOIN
                    moves as m ON ml.move=m.id
                LEFT JOIN
                    account_invoice as i ON m.invoice=i.id and ml.account=i.account
                LEFT JOIN
                    party_category_rel as pcr ON pcr.party=i.party
                LEFT JOIN
                    party_category as pc ON pc.id=pcr.category
                LEFT JOIN account_invoice_payment_term AS pt ON i.payment_term=pt.id
                LEFT JOIN party_party AS p ON i.party=p.id
            {where}
            GROUP BY m.id, m.invoice, i.party, p.name, p.id_number, pc.name, i.number, pt.name, i.invoice_date
            ORDER BY p.name DESC, i.invoice_date ASC"""

    @classmethod
    def get_query_maturity_invoice(cls, data, where):
        where += ' and i.account=ml.account'
        date_ = Pool().get('ir.date').today().strftime("'%Y-%m-%d'")
        if data['date_to']:
            # date_to = data['date_to']
            # where += f' and ml.maturity_date <= {date_to}'
            date_ = data['date_to']

        return f"""WITH moves AS (
            SELECT
                id,
                date,
                SPLIT_PART(origin, ',', 2)::INTEGER as invoice
            FROM
                account_move
            WHERE
                state='posted'
                AND origin LIKE 'account.invoice,%'
        ),
        party_category_rel AS (SELECT DISTINCT ON (party) party, category from party_category_rel),
        filtered_moves AS (
            SELECT
                ml.move,
                ml.maturity_date
            FROM
                account_move_line AS ml
            LEFT JOIN
                moves AS m ON ml.move = m.id
            LEFT JOIN
                account_invoice AS i ON m.invoice = i.id
            LEFT JOIN
                party_category_rel as pcr ON pcr.party=i.party
            {where}
                AND i.account = ml.account
            ORDER BY
                ml.move,
                ml.maturity_date NULLS LAST
        )
        SELECT
            DISTINCT ON (move)
            move,
            maturity_date,
            {date_} - maturity_date AS dias_vencidos
        FROM
            filtered_moves
        ORDER BY
            move,
            maturity_date"""

    @classmethod
    def get_query_payments(cls, data, where):
        if data['date_to']:
            date_to = data['date_to']
            where += f' and m.date <= {date_to}'

        return f"""
            with
                party_category AS (SELECT DISTINCT ON (party) party, category from party_category_rel)
            SELECT
                p.invoice, sum(ml.debit-ml.credit) AS paid
            FROM account_move_line as ml
                LEFT JOIN
                    "account_invoice-account_move_line" as p ON p.line=ml.id
                LEFT JOIN
                    account_move as m on ml.move=m.id
                LEFT JOIN
                    account_invoice as i on p.invoice=i.id
                LEFT JOIN
                    party_category as pcr ON pcr.party=i.party
            {where}
            GROUP BY p.invoice"""

    @classmethod
    def get_query_payments_reconcile(cls, data, where):
        if data['date_to']:
            date_to = data['date_to']
            where += f' and m.date <= {date_to}'
        return f"""WITH
            moves AS (
                SELECT
                    id, date, SPLIT_PART(origin, ',', 2)::INTEGER as invoice
                FROM
                    account_move
                WHERE state='posted' and origin LIKE 'account.invoice,%'),
            party_category AS (SELECT DISTINCT ON (party) party, category from party_category_rel),
            move_lines AS (SELECT ml.id, ml.reconciliation, m.invoice
                FROM
                    account_move_line as ml
                    LEFT JOIN moves as m ON ml.move=m.id
                    LEFT JOIN account_invoice as i ON m.invoice=i.id
                        and ml.account=i.account
                    LEFT JOIN party_category as pcr ON pcr.party=i.party
                {where})
            SELECT r.invoice, sum(ml.debit-ml.credit) FROM account_move_line as ml
            RIGHT JOIN move_lines as r on r.reconciliation=ml.reconciliation and r.id!=ml.id
            LEFT JOIN
                "account_invoice-account_move_line" as p ON p.line=ml.id
            where p.line IS NULL
            GROUP BY r.invoice;"""

    @classmethod
    def get_data_opening_balances(cls, data, cursor, labels, intervals):
        query_opening_balances = cls.get_query_opening_balances(data)
        print(query_opening_balances, 'this is query')
        cursor.execute(query_opening_balances)
        data_opening_balances = cursor.fetchall()
        columns = [
            'line', 'move', 'TERCERO', 'ID DOCUMENTO',
            'CATEGORIA', 'NUM. FACTURA', 'PLAZO DE PAGO', 'FECHA FACTURA',
            'FECHA VENCIMIENTO', 'DIAS VENCIDOS', 'VALOR FACTURA',
            'PAGO', 'SALDO',
            ]

        df = pd.DataFrame(data_opening_balances, columns=columns)

        df['PAGO'] = df['PAGO'].fillna(0)
        df['FECHA FACTURA'] = df['FECHA FACTURA'].astype(str)
        df['FECHA VENCIMIENTO'] = df['FECHA VENCIMIENTO'].astype(str)
        df = df.loc[df['SALDO'] > 0]

        # Crear una nueva columna con las etiquetas de los intervalos
        df['interval'] = pd.cut(df['DIAS VENCIDOS'], bins=intervals, labels=labels, right=False)

        # Crear columnas separadas para cada intervalo y asignar el valor de factura
        for label in labels:
            df[label] = df['SALDO'] * (df['interval'] == label).astype(int)

        columns = [
            'TERCERO', 'ID DOCUMENTO', 'CATEGORIA', 'NUM. FACTURA', 'PLAZO DE PAGO',
            'FECHA FACTURA', 'FECHA VENCIMIENTO', 'DIAS VENCIDOS',
            'VALOR FACTURA', 'PAGO', 'SALDO',
        ]
        columns.extend(labels)
        df = df.reindex(columns=columns)
        # df.drop(columns=['move', 'line', 'interval'], inplace=True)
        return df

    @classmethod
    def get_query_opening_balances(cls, data):
        # if data['detailed']:
        category = ''
        if data['category_party']:
            cat_ids = str(tuple(data['category_party'])).replace(',', '') if len(
                data['category_party']) == 1 else str(tuple(data['category_party']))
            category = f"and pcr.category in {cat_ids} "

        date_ = ''
        if data['date_to']:
            date_ = 'and am.date <= {}'.format(data['date_to'])

        accounts = ''
        if data.get('accounts'):
            field_accounts = tuple(data['accounts'])
            if len(data['accounts']) == 1:
                field_accounts = str(tuple(data['accounts'])).replace(',', '')

            accounts = f" and i.account in {field_accounts} "

        type_ = "and at.receivable='t'"
        if data['kind'] == 'in':
            type_ = "and at.receivable='t'"

        query_opening_collection = f"""
            WITH
                party_category_rel AS (SELECT DISTINCT ON (party) party, category FROM party_category_rel)
            SELECT ml.id, ml.move, pp.name, pp.id_number, pc.name AS category,
                ml.reference, ml.description, am.date, ml.maturity_date,
                (current_date - ml.maturity_date :: date) AS expired_days,
                (ml.debit - ml.credit) AS amount,
                COALESCE(sum(av.amount),0) AS payment_amount,
                ((ml.debit - ml.credit) - COALESCE(sum(av.amount),0)) AS total
            FROM
                account_move_line AS ml
                LEFT JOIN account_account AS ac ON ml.account = ac.id
                LEFT JOIN account_account_type AS at ON ac.type = at.id
                LEFT JOIN account_voucher_line AS av ON ml.id = av.move_line
                LEFT JOIN account_move AS am ON am.id = ml.move
                LEFT JOIN party_party AS pp ON pp.id = ml.party
                LEFT JOIN party_category_rel AS pcr ON pcr.party = pp.id
                LEFT JOIN party_category AS pc ON pcr.category = pc.id
            WHERE
                ac.reconcile='t'
                {type_}
                {accounts}
                {category}
                {date_}
                AND ml.maturity_date IS NOT NULL
                AND (am.origin IS NULL or am.origin like 'account.note%')
                AND ml.reconciliation IS NULL
            GROUP BY ml.id, ml.move, pp.name, pp.id_number, pc.name,
                ml.reference, ml.description, am.date, ml.maturity_date,
                expired_days, ml.debit, ml.credit;
        """
        return query_opening_collection
